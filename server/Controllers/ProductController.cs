using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using server.Model;

namespace server.Controllers {
    [ApiController]
    [Route ("[controller]")]
    public class ProductController : ControllerBase {
        public AppDbContext _context;
        public ProductController (AppDbContext context) {
            _context = context;
        }

        [HttpGet]
        public ActionResult getProducts () {
            var products = _context.Products.ToList ();
            return Ok (products);
        }

        [HttpPost]
        public ActionResult<Product> PostTodoItem (Product product) {
            _context.Products.Add (product);
            _context.SaveChanges ();

            return Ok (product);
        }
    }
}